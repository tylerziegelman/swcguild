function validateForm() {
	
	/**** Name and one form of contact information (Email or Phone) should be
filled in.  ****/
	
	var valFName = document.forms["contactForm"]["firstName"].value;
	var valEmail = document.forms["contactForm"]["Email"].value;
	var valPhone = document.forms["contactForm"]["Phone"].value;
	
	
	
	if(valFName == null || valFName == "") {
		alert("Please fill out name field");
		return false;
	}
	
	if (valEmail == "" && valPhone == ""){
		alert("Please provide an email address or phone number")
		return false;
	}
	
	/**** If Reason for Inquiry’s dropdown is selected to Other, make sure that
the Additional Information is filled in.  *****/
	
	var valOther = document.getElementById("inquiry");
	var valTextbox = document.forms["contactForm"]["Additional Information"].value;
    
	if (valOther.options[valOther.selectedIndex].value == 'Other' && valTextbox == ""){
          alert("Please provide additional information");
          return false;
		 }
	
	/**** Best days to contact you must have at least one day checked.  ****/
   	
	var valCheckBox = document.getElementsByName("day[]");
	var isChecked = false;
	
	for (var i = 0; i < valCheckBox.length; i++) {
		if (valCheckBox[i].checked) {
			isChecked = true;
			break;
		}
	}
	if (isChecked == false) {
		alert("Please select the best day(s) for us to contact you");
		return false;
	}
	
	return true;
}

	
